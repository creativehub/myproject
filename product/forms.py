from django import forms
from django.forms import ModelForm
from .models import Product




class ProductForm(ModelForm):
	class Meta:
		model = Product
		widgets={
			"name":forms.TextInput(attrs={'class':'form-control',}),
			"color":forms.TextInput(attrs={'class':'form-control',}),
			"taste":forms.TextInput(attrs={'class':'form-control',}),
			"amount":forms.NumberInput(attrs={'class':'form-control',}),
			"photo":forms.FileInput(attrs={'class':'form-control',}),
		}
		fields ="__all__"