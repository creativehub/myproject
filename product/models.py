from django.db import models

# Create your models here.
class Product(models.Model):

	name=models.CharField(max_length=100)
	color=models.CharField(max_length=200)
	taste=models.CharField(max_length=250)
	photo=models.FileField(upload_to='emp',null=True,blank=True)
	amount = models.DecimalField(max_digits=20,decimal_places=4)