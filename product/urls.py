from django.conf.urls import url
from . import views
app_name = "product"

urlpatterns = [
    url(r'^create/$', views.create, name='create'),
    url(r'^$', views.post_list, name='post_list'),
    url(r'^(?P<pk>[^/]+)/edit/$',views.edit,name='edit'),
]