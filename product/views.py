from django.shortcuts import render

# Create your views here.
from .models import Product
from .forms import ProductForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse,reverse_lazy

def post_list(request):
	products=Product.objects.all()
	context={'products':products}
	return render(request, 'product/product_list.html',context) 

def create(request):
	form = ProductForm(request.POST or None,request.FILES or None)
	if request.method=="POST" and form.is_valid():
		form.save()
		return HttpResponseRedirect(reverse('product:post_list'))
	context={'form':form,'form_url':reverse_lazy('product:create')}
	return render(request, 'product/create.html',context) 

def edit(request,pk):
	products=Product.objects.get(pk=pk)
	if request.method=="POST": 
		print(request.FILES)
		form = ProductForm(request.POST,request.FILES or None,instance=products)
		if form.is_valid():
			product=form.save()
			product.save()
			return HttpResponseRedirect(reverse('product:post_list'))
	else:
		form=ProductForm(instance=products)
	context={'form':form,'form_url':reverse_lazy('product:edit', args=[products.pk])}
	return render(request, 'product/create.html',context) 
